<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_exela');

/** MySQL database username */
define('DB_USER', 'exela_user');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WP_ENV', 'development');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ':)eA/ wJa4!j1avqV<t(qk+OnXB(KrGVJ ~p+Bdsr2fh3p&.jL0|heO_dFKL6sjq');
define('SECURE_AUTH_KEY',  'X$51R{)-t-0Kd_fqB$<G;GJ1Fj--.R!Q@Nlehr+(3?eP}Ctmp0o|{ClDFG<Twb8w');
define('LOGGED_IN_KEY',    'qS!@)]]*!8YZ;-J;!gd3?5$i-Ksx)Mq^^ytD{^9[qFMd,kb/LWRJ,3RS|mTBJmyu');
define('NONCE_KEY',        'HdX[Uo@KpaYC~}=yb-S<DnAibKBeXWi|op38J29It>si%WVJXBgR(2bcBPNsP^f,');
define('AUTH_SALT',        'xW{{> >*#O%9w8caYi9>6=B-`Fu^+S$e|U#Ks.TNK=Hu%<<Xqu#nV(zejP;{-!me');
define('SECURE_AUTH_SALT', 'L52pRn,y,jHzy6%M>Wvr9FmrsHC&eb+j|1DqTmam`g=^g!prju FmaEz[~.D;:{(');
define('LOGGED_IN_SALT',   '0=Gr-sccN-!(g|8N6cg*f@c(_}G?7Oa<&|yhrb$p:y-Fp{PJWNb,n0;|-9]{DyGi');
define('NONCE_SALT',       'c+9-;UhDOq24i!Rq|XLbQ @^qmyp&|RtaoHn8(Zp!:P^6Mu]t4aQ~M+-xe]G54 j');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
